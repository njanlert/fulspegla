package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/ronbu/fsevents"
)

const version = "0.1"

type patternList []*Glob

func (pl *patternList) Set(value string) error {
	for _, pattern := range strings.Split(value, ",") {
		glob, err := Compile(pattern)
		if err != nil {
			return err
		}
		*pl = append(*pl, glob)
	}
	return nil
}

func (pl *patternList) String() string {
	s := make([]string, len(*pl))
	for i, p := range *pl {
		s[i] = p.String()
	}
	return strings.Join(s, ",")
}

var (
	ignored patternList = patternList{MustCompile(".*")}
	local   string
	remote  string
	verbose bool
)

func init() {
	flag.Var(&ignored, "ignore", "comma-separated list of filename patterns to ignore")
	flag.BoolVar(&verbose, "verbose", false, "verbose logging")
}

func isIgnored(path string) bool {
	for _, p := range ignored {
		if p.Match(path) {
			return true
		}
	}
	return false
}

func copyFile(dst, src string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	defer in.Close()
	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return out.Sync()
}

func shouldSync(e fsevents.Event) bool {
	if e.Flags&fsevents.EF_MUSTSCANSUBDIRS != 0 {
		log.Println("WARNING! EF_MUSTSCANSUBDIRS flag not supported -- folder possibly out of sync", e)
	}
	if e.Flags&(fsevents.EF_CREATED|fsevents.EF_REMOVED|fsevents.EF_RENAMED|fsevents.EF_MODIFIED) == 0 {
		return false
	}
	subpath, err := filepath.Rel(local, e.Path)
	if err != nil {
		panic(err)
	}
	if isIgnored(subpath) {
		return false
	}
	return true
}

// enqueue appends events to the queue, given that the events need to be synced
// and that no other event for the file is already present in the queue
func enqueue(queue, extra []fsevents.Event) []fsevents.Event {
	enqueued := make(map[string]struct{})
	for _, e := range queue {
		enqueued[e.Path] = struct{}{}
	}
	for _, e := range extra {
		if _, exists := enqueued[e.Path]; !exists && shouldSync(e) {
			queue = append(queue, e)
			enqueued[e.Path] = struct{}{}
		}
	}
	return queue
}

func mirror(e fsevents.Event) error {
	subpath, err := filepath.Rel(local, e.Path)
	if err != nil {
		panic(err)
	}
	// We *do* get a flag signalling whether the file was created, modified, removed etc...
	// However, deleting a file and then re-creating it results in the same flag set as when
	// creating a file and then deleting it... Simplest solution seems to just be check the
	// local state.
	fi, err := os.Stat(e.Path)
	if err == nil {
		if verbose {
			log.Println("+", subpath)
		}
		if fi.IsDir() {
			err = os.Mkdir(filepath.Join(remote, subpath), fi.Mode())
		} else {
			err = copyFile(filepath.Join(remote, subpath), filepath.Join(local, subpath))
		}
	} else if os.IsNotExist(err) {
		if verbose {
			log.Println("-", subpath)
		}
		err = os.Remove(filepath.Join(remote, subpath))
		if os.IsNotExist(err) {
			err = nil
		}
	}
	return err
}

func coalesce(q []fsevents.Event, events <-chan []fsevents.Event) <-chan fsevents.Event {
	out := make(chan fsevents.Event)
	go func() {
		// Consume queue until empty -- until then make sure there are no duplicates
		for len(q) > 0 {
			select {
			case extra, ok := <-events:
				if !ok {
					// event channel is closed, so we should gracefully abort
					break
				}
				q = enqueue(q, extra)
			case out <- q[0]:
				q = q[1:len(q)]
			}
		}
		close(out)
	}()
	return out
}

func watch(events <-chan []fsevents.Event) <-chan struct{} {
	done := make(chan struct{})
	go func() {
		for chunk := range events {
			q := enqueue(nil, chunk)
			if len(q) == 0 {
				continue
			}
			log.Println("sync started")
			sync := coalesce(q, events)

			n := 0
			for e := range sync {
				if err := mirror(e); err != nil {
					log.Println("warning:", err)
				} else {
					n++
				}
			}
			log.Printf("sync finished (%d file(s))\n", n)
		}
		close(done)
	}()
	return done
}

func fileArg(n int) (string, error) {
	p, err := filepath.Abs(flag.Arg(n))
	if err != nil {
		return p, err
	}
	_, err = os.Stat(p)
	return p, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "fulspegla %s - sync changes from one directory to another\n\n", version)
		fmt.Fprintf(os.Stderr, "usage: %s [options] source target\n\noptions:\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
	if flag.NArg() != 2 {
		flag.Usage()
		os.Exit(1)
	}
	var err error
	local, err = fileArg(0)
	if err != nil {
		log.Fatal(err)
	}
	remote, err = fileArg(1)
	if err != nil {
		log.Fatal(err)
	}
	stream := fsevents.New(0, fsevents.NOW, 200*time.Millisecond, fsevents.CF_FILEEVENTS, local)
	log.Printf("starting: sync %v to %v\n", local, remote)
	stream.Start()
	defer stream.Close()

	done := watch(stream.Chan)
	kill := make(chan os.Signal, 1)
	signal.Notify(kill, syscall.SIGTERM, os.Kill)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	interrupted := false

loop:
	for {
		select {
		case s := <-kill:
			log.Printf("%v: forcibly exiting\n", s)
			break loop
		case <-interrupt:
			if interrupted {
				log.Println("second interrupt: forcibly exiting")
				break loop
			} else {
				log.Println("interrupt: shutting down")
				interrupted = true
				close(stream.Chan)
				continue loop
			}
		case <-done:
			break loop
		}
	}
}
