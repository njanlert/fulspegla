package main

import "testing"

func TestCompile(t *testing.T) {
	check := func(glob string, pos, neg []string) {
		g, err := Compile(glob)
		if err != nil {
			t.Errorf("Compile(%#v) returned %v", glob, err)
			return
		} else if g == nil {
			t.Errorf("Compile(%#v) returned nil", glob)
			return
		}
		for _, s := range pos {
			if !g.Match(s) {
				t.Errorf("%v did not match %v, but should", g, s)
			}
		}
		for _, s := range neg {
			if g.Match(s) {
				t.Errorf("%v matched %v, but shouldn't", g, s)
			}
		}
	}
	checkErr := func(glob string) {
		_, err := Compile(glob)
		if err == nil {
			t.Errorf("Compile(%#v) should return an error, but didn't", glob)
		}
	}

	check("foo", []string{"foo", "/foo", "foo/", "bar/foo", "foo/bar"}, []string{"foobar"})
	check("foo*bar", []string{"foobar", "fooXXXbar"}, []string{"foo", "bar", "foo/bar"})
	check("foo?bar", []string{"fooXbar"}, []string{"foo", "bar", "foobar", "foo/bar"})
	check(`foo\*bar`, []string{"foo*bar"}, []string{"fooXbar"})
	check("py[co]", []string{"pyc", "pyo"}, []string{"py", "pyd", "py[co]"})
	check("foo[ac-d]x", []string{"fooax", "foocx", "foodx"}, []string{"foox", "foobx", "fooa"})
	check("foo[^x]", []string{"fooa", "fooz"}, []string{"foo", "foox"})
	check(`foo[x\]]`, []string{"foox", "foo]"}, []string{"foo["})

	check("foo.bar", []string{"foo.bar"}, []string{"fooXbar"})
	check("foo(bar", []string{"foo(bar"}, []string{})
	check("foo]bar", []string{"foo]bar"}, []string{})
	check(`\pN`, []string{"pN"}, []string{"1"})
	check(`[\pN]`, []string{"p", "N"}, []string{"1"})
	check("[[:alpha:]]", []string{"a]"}, []string{"X"})

	checkErr("")
	checkErr(`foo\`)
	checkErr("foo[]")
}
