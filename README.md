fulspegla
=========
Sync changes from one directory to another. Only works on Mac OS X, since it
uses the File System Events API.

Example usage:

    fulspegla ~/ppcode/local-nij /Volumes/pw-next/nij
