package main

import (
	"bytes"
	"errors"
	"regexp"
)

type Glob struct {
	expr string
	re   *regexp.Regexp
}

var BadSyntax = errors.New("bad syntax")

var specialBytes = []byte(`\.*?+()|[]{}^$`)

func special(b byte) bool {
	return bytes.IndexByte(specialBytes, b) >= 0
}

// charRange returns a parsed character range and number of bytes consumed
func charRange(s string) ([]byte, int) {
	cr := make([]byte, len(s)*2)

	cr[0] = s[0] // so the initial [ is not escaped
	i := 1
	j := 1
	closed := false
	for ; i < len(s) && !closed; i++ {
		switch s[i] {
		case ']':
			closed = true
		case '[': // escape to avoid character classes
			cr[j] = '\\'
			j++
		case '\\':
			if i+1 >= len(s) {
				return nil, 0
			}
			esc := s[i+1]
			if esc == ']' || esc == '-' || esc == '\\' {
				cr[j] = '\\'
				j++
			}
			i++
		}
		cr[j] = s[i]
		j++
	}
	cr = cr[:j]
	if len(cr) == 0 || !closed {
		return nil, 0
	}
	return cr, i
}

func Compile(expr string) (*Glob, error) {
	if len(expr) == 0 {
		return nil, BadSyntax
	}

	p := make([]byte, 0, len(expr)*2)

	// A byte loop is correct because all metacharacters are ASCII.
	for i := 0; i < len(expr); i++ {
		switch c := expr[i]; {
		case c == '*':
			p = append(p, []byte(`[^/]*`)...)
		case c == '?':
			p = append(p, []byte(`[^/]`)...)
		case c == '\\':
			if i+1 >= len(expr) {
				return nil, BadSyntax
			}
			if esc := expr[i+1]; special(esc) { // so to avoid \d \pN etc.
				p = append(p, '\\', esc)
			} else {
				p = append(p, esc)
			}
			i++
		case c == '[':
			cr, n := charRange(expr[i:])
			if n == 0 {
				return nil, BadSyntax
			}
			p = append(p, cr...)
			i += n - 1
		case special(c):
			p = append(p, '\\', c)
		default:
			p = append(p, c)
		}
	}

	re, err := regexp.Compile(`(?:^|/)` + string(p) + `(?:$|/)`)
	if err != nil {
		return nil, err
	}
	return &Glob{expr, re}, nil
}

func MustCompile(expr string) *Glob {
	g, err := Compile(expr)
	if err != nil {
		panic(err)
	}
	return g
}

func (g *Glob) String() string {
	return g.expr
}

func (g *Glob) Match(s string) bool {
	return g.re.MatchString(s)
}
